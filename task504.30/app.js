import { Product } from "./product.js";
import { Album } from "./album.js";
import { Movie } from "./movie.js";

var vProduct = new Product("Product1", 20, 10);
console.log(vProduct);
vProduct.sellCopies();
vProduct.orderCopies();
console.log(vProduct instanceof Product);

console.log("--------------------------");
var vAlbum1 = new Album("Product2", 30, 15, "Artist1");
console.log(vAlbum1);
vAlbum1.sellCopies();
vAlbum1.orderCopies();
console.log(vAlbum1 instanceof Album);
console.log(vAlbum1 instanceof Product);

console.log("--------------------------");
var vMovie1 = new Movie("Product3", 50, 20, "Director1");
console.log(vMovie1);
vMovie1.sellCopies();
vMovie1.orderCopies();
console.log(vMovie1 instanceof Movie);
console.log(vMovie1 instanceof Product);