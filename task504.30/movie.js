import { Product } from "./product.js";

class Movie extends Product {
    _director;

    constructor(paramName, paramPrice, paramNumberOfCopies, paramDirector) {
        super(paramName, paramPrice, paramNumberOfCopies);
        this._director = paramDirector;
    }
}

export { Movie }