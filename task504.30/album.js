import { Product } from "./product.js";

class Album extends Product {
    _artist;

    constructor(paramName, paramPrice, paramNumberOfCopies, paramArtist) {
        super(paramName, paramPrice, paramNumberOfCopies);
        this._artist = paramArtist;
    }
}

export { Album }