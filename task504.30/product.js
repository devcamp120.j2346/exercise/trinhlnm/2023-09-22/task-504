class Product {
    _name;
    _price;
    _numberOfCopies;

    constructor(paramName, paramPrice, paramNumberOfCopies) {
        this._name = paramName;
        this._price = paramPrice;
        this._numberOfCopies = paramNumberOfCopies;
    }

    sellCopies() {
        console.log("Sell copies of " + this._name);
    }

    orderCopies() {
        console.log("Order copies of " + this._name);
    }
}

export { Product }