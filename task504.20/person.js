class Person {
    _personName;
    _personAge;
    _gender;

    constructor(paramPersonName, paramPersonAge, paramGender) {
        this._personName = paramPersonName;
        this._personAge = paramPersonAge;
        this._gender = paramGender;
    }

    getPersonInfo() {
        console.log("Name: " + this._personName);
        console.log("Age: " + this._personAge);
        console.log("Gender: " + this._gender);
    }
}

export { Person }