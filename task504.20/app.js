import { Person } from "./person.js";
import { Employee } from "./employee.js";

var vPerson1 = new Person("Name1", "18", "Nam");
var vPerson2 = new Person("Name2", "19", "Nu");
var vPerson3 = new Person("Name3", "23", "Nam");
vPerson1.getPersonInfo();
vPerson2.getPersonInfo();
vPerson3.getPersonInfo();
console.log(vPerson1 instanceof Person);

var vEmployee1 = new Employee("Name4", "18", "Nam", "Employer1", "100", "Position1");
var vEmployee2 = new Employee("Name5", "19", "Nu", "Employer2", "80", "Position2");
var vEmployee3 = new Employee("Name6", "23", "Nam", "Employer3", "120", "Position3");

console.log("-------------------");
vEmployee1.getPersonInfo();
vEmployee1.getEmployeeInfo();
vEmployee1.setSalary("300");
vEmployee1.setPosition("Position11");
console.log(vEmployee1.getSalary());
console.log(vEmployee1.getPosition());

console.log("-------------------");
vEmployee2.getPersonInfo();
vEmployee2.getEmployeeInfo();
vEmployee2.setSalary("400");
vEmployee2.setPosition("Position21");
console.log(vEmployee2.getSalary());
console.log(vEmployee2.getPosition());

console.log("-------------------");
vEmployee3.getPersonInfo();
vEmployee3.getEmployeeInfo();
vEmployee3.setSalary("500");
vEmployee3.setPosition("Position31");
console.log(vEmployee3.getSalary());
console.log(vEmployee3.getPosition());
vEmployee3.getEmployeeInfo();

console.log(vEmployee1 instanceof Employee);
console.log(vEmployee1 instanceof Person);