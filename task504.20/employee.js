import { Person } from "./person.js";

class Employee extends Person {
    _employer;
    _salary;
    _position;

    constructor(
        paramPersonName,
        paramPersonAge,
        paramGender,
        paramEmployer,
        paramSalary,
        paramPosition
    ) {
        super(paramPersonName, paramPersonAge, paramGender);
        this._employer = paramEmployer;
        this._salary = paramSalary;
        this._position = paramPosition;
    }

    getSalary() {
        return this._salary;
    }

    getPosition() {
        return this._position;
    }

    setSalary(paramSalary) {
        this._salary = paramSalary;
    }

    setPosition(paramPosition) {
        this._position = paramPosition;
    }

    getEmployeeInfo() {
        console.log("Employer: " + this._employer);
        console.log("Salary: " + this._salary);
        console.log("Position: " + this._position);
    }
}

export { Employee }