import { Car } from "./car.js";
import { ElectricCar } from "./electricCar.js";
import { PetrolCar } from "./petrolCar.js";

var vCar = new Car("Make1", "Model1");
console.log(vCar);
vCar.drive();
console.log(vCar instanceof Car);

console.log("--------------------------");
var vECar1 = new ElectricCar("Make2", "Model2", "Battery Level 1");
console.log(vECar1);
vECar1.drive();
vECar1.charge();
console.log(vECar1 instanceof ElectricCar);
console.log(vECar1 instanceof Car);

console.log("--------------------------");
var vPCar1 = new PetrolCar("Make3", "Model3", "Fuel Level 1");
console.log(vPCar1);
vPCar1.drive();
vPCar1.fillUp();
console.log(vPCar1 instanceof PetrolCar);
console.log(vPCar1 instanceof ElectricCar);
console.log(vPCar1 instanceof Car);