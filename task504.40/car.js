class Car {
    _make;
    _model;

    constructor(paramMake, paramModel) {
        this._make = paramMake;
        this._model = paramModel;
    }

    drive() {
        console.log(this._make + " driving");
    }
}

export { Car }