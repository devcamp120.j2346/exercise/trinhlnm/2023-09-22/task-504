import { Car } from "./car.js";

class PetrolCar extends Car {
    _fuelLevel;

    constructor(paramMake, paramModel, paramFuelLevel) {
        super(paramMake, paramModel);
        this._fuelLevel = paramFuelLevel;
    }

    fillUp() {
        console.log(this._fuelLevel + " filling up");
    }
}

export { PetrolCar }