import { Car } from "./car.js";

class ElectricCar extends Car {
    _batteryLevel;

    constructor(paramMake, paramModel, paramBatteryLevel) {
        super(paramMake, paramModel);
        this._batteryLevel = paramBatteryLevel;
    }

    charge() {
        console.log(this._batteryLevel + " charging");
    }
}

export { ElectricCar }