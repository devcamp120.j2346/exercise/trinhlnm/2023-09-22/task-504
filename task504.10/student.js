import { Person } from "./person.js";

class Student extends Person {
    _standard;
    _collegeName;
    _grade;

    constructor(
        paramPersonName,
        paramPersonAge,
        paramGender,
        paramStandard,
        paramCollegeName,
        paramGrade
    ) {
        super(paramPersonName, paramPersonAge, paramGender);
        this._standard = paramStandard;
        this._collegeName = paramCollegeName;
        this._grade = paramGrade;
    }

    getGrade() {
        return this._grade;
    }

    setGrade(paramGrade) {
        this._grade = paramGrade;
    }

    getStudentInfo() {
        console.log("-----Student-----");
        console.log("Standard: " + this._standard);
        console.log("College Name: " + this._collegeName);
        console.log("Grade: " + this._grade);
        console.log("----------------");
    }
}

export {Student}
