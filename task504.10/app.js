import { Person } from "./person.js";
import { Student } from "./student.js";

var vPerson1 = new Person("Name1", "18", "Nam");
var vPerson2 = new Person("Name2", "19", "Nu");
var vPerson3 = new Person("Name3", "23", "Nam");
vPerson1.getPersonInfo();
vPerson2.getPersonInfo();
vPerson3.getPersonInfo();
console.log(vPerson1 instanceof Person);

var vStudent1 = new Student("Name4", "18", "Nam", "Standard1", "College Name 1", "Grade1");
var vStudent2 = new Student("Name5", "19", "Nu", "Standard2", "College Name 2", "Grade2");
var vStudent3 = new Student("Name6", "23", "Nam", "Standard3", "College Name 3", "Grade3");

vStudent1.setGrade("Grade15");
console.log(vStudent1.getGrade());
vStudent1.getStudentInfo();
vStudent1.getPersonInfo();

console.log(vStudent2.getGrade());
vStudent2.setGrade("Grade16");
vStudent2.getStudentInfo();
vStudent2.getPersonInfo();

console.log(vStudent3.getGrade());
vStudent3.setGrade("Grade16");
vStudent3.getPersonInfo();
vStudent3.getStudentInfo();

console.log(vStudent1 instanceof Student);
console.log(vStudent1 instanceof Person);