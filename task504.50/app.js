import { Employee } from "./employee.js";
import { CommissionEmployee } from "./commissionEmployee.js";
import { HourlyEmployee } from "./hourlyEmployee.js";
import { SalariedEmployee } from "./salariedEmployee.js";
import { BasePlusCommissionEmployee } from "./basePlusCommissionEmployee.js";

var vEmployee1 = new Employee("firstName1", "lastName1", "socialSecurityNumber1");
console.log(vEmployee1);
vEmployee1.testFun();
console.log(vEmployee1 instanceof Employee);

var vCommissionEmployee1 = new CommissionEmployee("firstName2", "lastName2", "socialSecurityNumber2", "grossSales1", "commissionRate1");
console.log(vCommissionEmployee1);
vCommissionEmployee1.testFun();
vCommissionEmployee1.testFunCE();
console.log(vCommissionEmployee1 instanceof CommissionEmployee);
console.log(vCommissionEmployee1 instanceof Employee);

var vHourlyEmployee1 = new HourlyEmployee("firstName3", "lastName3", "socialSecurityNumber3", "wage1", "hours1");
console.log(vHourlyEmployee1);
vHourlyEmployee1.testFun();
vHourlyEmployee1.testFunHE();
console.log(vHourlyEmployee1 instanceof CommissionEmployee);
console.log(vHourlyEmployee1 instanceof HourlyEmployee);
console.log(vHourlyEmployee1 instanceof Employee);

var vSalariedEmployee1 = new SalariedEmployee("firstName4", "lastName4", "socialSecurityNumber4", "weeklySalary1");
console.log(vSalariedEmployee1);
vSalariedEmployee1.testFun();
vSalariedEmployee1.testFunSE();
console.log(vSalariedEmployee1 instanceof CommissionEmployee);
console.log(vSalariedEmployee1 instanceof HourlyEmployee);
console.log(vSalariedEmployee1 instanceof SalariedEmployee);
console.log(vSalariedEmployee1 instanceof Employee);

var vBasePlusCommissionEmployee1 = new BasePlusCommissionEmployee("firstName5", "lastName5", "socialSecurityNumber5", "grossSales2", "commissionRate2", "baseSalary1");
console.log(vBasePlusCommissionEmployee1);
vBasePlusCommissionEmployee1.testFun();
vBasePlusCommissionEmployee1.testFunCE();
vBasePlusCommissionEmployee1.testFunBPCE();
console.log(vBasePlusCommissionEmployee1 instanceof CommissionEmployee);
console.log(vBasePlusCommissionEmployee1 instanceof BasePlusCommissionEmployee);
console.log(vBasePlusCommissionEmployee1 instanceof HourlyEmployee);
console.log(vBasePlusCommissionEmployee1 instanceof SalariedEmployee);
console.log(vBasePlusCommissionEmployee1 instanceof Employee);