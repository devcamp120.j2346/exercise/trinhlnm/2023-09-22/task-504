import { Employee } from "./employee.js";
import { CommissionEmployee } from "./commissionEmployee.js";

class BasePlusCommissionEmployee extends CommissionEmployee {
    _baseSalary;

    constructor(paramFirstName, paramLastName, paramSSN, paramGrossSales, paramCommissionRate, paramBaseSalary) {
        super(paramFirstName, paramLastName, paramSSN, paramGrossSales, paramCommissionRate);
        this._baseSalary = paramBaseSalary;
    }

    testFunBPCE() {
        console.log(this._baseSalary + " is testing");
    }
}

export {BasePlusCommissionEmployee}