import { Employee } from "./employee.js";

class CommissionEmployee extends Employee {
    _grossSales;
    _commissionRate;

    constructor(paramFirstName, paramLastName, paramSSN, paramGrossSales, paramCommissionRate) {
        super(paramFirstName, paramLastName, paramSSN);
        this._grossSales = paramGrossSales;
        this._commissionRate = paramCommissionRate;
    }

    testFunCE() {
        console.log(this._grossSales + " is testing");
    }
}

export {CommissionEmployee}