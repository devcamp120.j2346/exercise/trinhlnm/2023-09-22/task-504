import { Employee } from "./employee.js";

class HourlyEmployee extends Employee {
    _wage;
    _hours;

    constructor(paramFirstName, paramLastName, paramSSN, paramWage, paramHours) {
        super(paramFirstName, paramLastName, paramSSN);
        this._wage = paramWage;
        this._hours = paramHours;
    }

    testFunHE() {
        console.log(this._wage + " is testing");
    }
}

export {HourlyEmployee}