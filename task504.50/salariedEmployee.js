import { Employee } from "./employee.js";

class SalariedEmployee extends Employee {
    _weeklySalary;

    constructor(paramFirstName, paramLastName, paramSSN, paramWeeklySalary) {
        super(paramFirstName, paramLastName, paramSSN);
        this._weeklySalary = paramWeeklySalary;
    }

    testFunSE() {
        console.log(this._weeklySalary + " is testing");
    }
}

export {SalariedEmployee}