class Employee {
    _firstName;
    _lastName;
    _socialSecurityNumber;

    constructor(paramFirstName, paramLastName, paramSSN) {
        this._firstName = paramFirstName;
        this._lastName = paramLastName;
        this._socialSecurityNumber = paramSSN;
    }

    testFun() {
        console.log(this._firstName + " is testing");
    }
}

export {Employee}